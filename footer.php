
<div id="footer">
	<div class="container" id="footerLinks">
		<div class="col-md-3 col-sm-6 col-xs-12">
			<h2>The links</h2>
			<ul>
				<li><a href="">test links</a></li>
				<li><a href="">test links</a></li>
				<li><a href="">test links</a></li>
				<li><a href="">test links</a></li>
				<li><a href="">test links</a></li>
				<li><a href="">test links</a></li>
			</ul>
		</div>
		<div class="col-md-3 col-sm-6 col-xs-12">
			<h2>The links</h2>
			<ul>
				<li><a href="">test links</a></li>
				<li><a href="">test links</a></li>
				<li><a href="">test links</a></li>
				<li><a href="">test links</a></li>
				<li><a href="">test links</a></li>
			</ul>
		</div>
		<div class="col-md-3 col-sm-6 col-xs-12">
			<h2>The links</h2>
			<ul>
				<li><a href="">link test</a></li>
				<li><a href="">link test</a></li>
				<li><a href="">link test</a></li>
				<li><a href="">link test</a></li>
				<li><a href="">link test</a></li>
				<li><a href="">link test</a></li>
			</ul>
		</div>
		<div class="col-md-3 col-sm-6 col-xs-12">
			<h2>The links</h2>
			<address>	
			Kathmandu (Head Office)<br>
			1259 Baneshowr GPO Box: 8975<br>
			EPC: 1234,Kathmandu, Nepal<br>
			Tel : +977 (1) 2123344<br>
			Email: info@kiec.edu.np<br>
			</address>
		</div>
		<div class="aff">
	<div class="afflogo social col-sm-6">
	<h3>Find Us</h3>
		<a href="https://www.facebook.com/#" class="facebook" target="_blank" rel="nofollow"></a>       
            <a href="https://twitter.com/#" class="twitter" target="_blank" rel="nofollow"></a>       
            <a href="https://plus.google.com/#" class="google" target="_blank" rel="nofollow"></a>            
            <a href="/feed" class="feed"></a>  	
		</div><!--end faid us-->
	</div>
	<div class="cpRight col-sm-6 ">
		<a href="#">Privacy Policy</a>|<a href="#">Terms & Conditions</a>|<a href="#">FAQ</a>|<a href="#">SitemapCopyright</a> <br>
		© 2011-13 Kathmandu Infosys. All Rights Reserved.<br>
		Maintained By : Arc Creatives

	</div>
		
	</div><!--end aff-->
	<div class="clearfix"></div>
	</div><!--end footerLinks-->
	</div><!--end containter-->
</div><!--end #footer-->

<?php wp_footer() ?>
<script>
(function($){
 $(document).ready(function(){
 $("ul.sub-menu").parent().addClass("dropdown");
 $("ul.sub-menu").addClass("dropdown-menu");
 $("ul#topMenu li.dropdown a").addClass("dropdown-toggle");
 $("ul.sub-menu li a").removeClass("dropdown-toggle"); 
 $('.navbar .dropdown-toggle').append('<b class="caret"></b>');
 $('a.dropdown-toggle').attr('data-toggle', 'dropdown');
 
 $('.navbar .dropdown').hover(function() {
  $(this).find('.dropdown-menu').first().stop(true, true).delay(250).slideDown();
}, function() {
  $(this).find('.dropdown-menu').first().stop(true, true).delay(100).slideUp()
});

 });
})(jQuery);
</script>
<script type="text/javascript">
(function($) {
	$(function() { //on DOM ready 
    		$("#scroller").simplyScroll();
	});
 })(jQuery);
</script>
</body>
</html>