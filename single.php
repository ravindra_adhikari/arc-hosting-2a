<?php get_header(); ?>

<div class="contain">
	<div class="container" id="contain">

	<div class="intro col-lg-8 col-sm-8">
	<?php
		if (have_posts()) : the_post(); ?>
	<h1><?php the_title(); ?></h1> 
		<?php the_content(); ?>
      <?php query_posts('posts_per_page=1');?>
		<?php endif; ?>
	</div>

	<div class="sidebar col-lg-4 col-sm-4">
		<?php if (function_exists ( dynamic_sidebar("Left Sidebar"))) : ?>
		<?php dynamic_sidebar ("Left Sidebar"); ?>
		<?php endif; ?>
	</div>
	<div class="clearfix"></div>

</div><!--end container-->

</div>


<?php get_footer(); ?>