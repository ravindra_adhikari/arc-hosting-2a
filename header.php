<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes() ?>><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8" <?php language_attributes() ?>><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9" <?php language_attributes() ?>><![endif]-->
<!--[if gt IE 8]><!--><html class="no-js" <?php language_attributes() ?>><!--<![endif]-->
    <head>
        <meta charset="<?php bloginfo( 'charset' ) ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width">
        <title><?php wp_title( '|', true, 'right' ) ?></title>
		<meta name="author" content="">
		<link rel="author" href="">
		<?php wp_head();?>
         <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/dist/styles/bootstrap.css">
         <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/dist/styles/main.css">
         <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,900,400italic,600italic' rel='stylesheet' type='text/css'>
         <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
         
       
	<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
         <link href='http://fonts.googleapis.com/css?family=Scada:700italic,400,700' rel='stylesheet' type='text/css'>

<!-- Latest compiled and minified JavaScript -->
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.simplyscroll.js"></script>
    </head>
    <body <?php body_class() ?>>

   <div class="" id="header">
   	<div class="container">
   		<div class="logo">
   		<a href="#"><h1>Magna Eduction Consultancy Pvt. Ltd.</h1></a>
   		</div><!--end logo-->
   	</div><!--end conatainer-->
   </div><!--end header -->



<nav class="navbar navbar-default" role="navigation">
<div class="container">

  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
  <?php
     wp_nav_menu( array( 
     'theme_location' => 'primary',
     'menu' => 'topMenu', 
     'menu_class' => 'nav navbar-nav navbar-right', 
     'menu_id' => 'topMenu' ) 
     );
 ?> 
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
  </div><!--end container-->
</nav>

<div id="banner">
	<div class="container">
		<div class="slider col-md-9 col-sm-12" >
			<div class="clearfix"></div>
  <div id="homeSlider" class="clearfix flexslider">
     <?php if(function_exists('show_flexslider_rotator')) echo show_flexslider_rotator('homepage' ); ?>   
   </div>
		</div>
		<div class="banner col-md-3 col-sm-12">
			<div class="banners col-sm-4 col-md-12 col-lg-12">Banner 1</div>
			<div class="banners col-sm-4 col-md-12 col-lg-12">Banner 2</div>
			<div class="banners col-sm-4 col-md-12 col-lg-12">Banner 3</div>
		</div>
	</div>
</div>




