function slideSwitch(slide) {
    var $active = $('#story .stories.active');

    if ( $active.length == 0 ) $active = $('.stories:last');

    // use this to pull the images in the order they appear in the markup
    var $next =  $active.next().length ? $active.next() : $('#story .stories:first');
	var $prev =  $active.prev().length ? $active.prev() : $('#story .stories:last');
        

    // uncomment the 3 lines below to pull the images in random order
    
     //var $sibs  = $active.siblings();
     //var rndNum = Math.floor(Math.random() * $sibs.length );
     //var $next  = $( $sibs[ rndNum ] );


    $active.addClass('last-active');
	if(slide == "next"){
		$next.css({opacity: 0.0})
			.addClass('active')
			.animate({opacity: 1.0}, 100, function() {
				$active.fadeIn();
				$active.removeClass('active last-active');
			});
	}else if (slide == "prev"){
			$prev.css({opacity: 0.0})
			.addClass('active')
			.animate({opacity: 1.0}, 100, function() {
				$active.fadeIn();
				$active.removeClass('active last-active');
			});
	
	}else{
			$next.css({opacity: 0.0})
			.addClass('active')
			.animate({opacity: 1.0}, 100, function() {
				$active.fadeIn();
				$active.delay(5000).fadeOut();
				$active.removeClass('active last-active');
			});
	}	
}

$(function() {
    setInterval( "slideSwitch()", 5000 );
});

$( document ).ready(function() {
	$(".prevNav").click(function(){
		event.preventDefault();
		slideSwitch("prev");
		console.log( "previous");
	});
	
	$(".nextNav").click(function(){
		event.preventDefault();
		slideSwitch("next");
		console.log( "next" );
		
	});
	
});