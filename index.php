<?php get_header(); ?>
<div class="row">
<div class="container" id="promo">
	<div class="col-lg-4 col-sm-4 col-sm-12">
		<div class="promos"></div>
	</div>
	<div class="col-lg-4 col-sm-4 col-sm-12">
		<div class="promos"></div>
	</div>
	<div class="col-lg-4 col-sm-4 col-sm-12">
		<div class="promos">
			testing the for test
		</div>
	</div>
</div>
</div><!--end rows-->

<div class="row">
<div class="container" id="intro">
	<div class="col-md-8">
		<div class="intro">
			<h2>hello hi welcome</h2>
			<p>Education in its general sense is a form of learning in which the knowledge, skills, and habits of a group of people are transferred from one generation to the next through teaching, training, or research. Education frequently takes place under the guidance of others, but may also be autodidactic.[1] Any experience that has a formative effect on the way one thinks, feels, or acts may be considered educational. Education is commonly divided into stages such as preschool, primary school, secondary school and then college, university or apprenticeship.</p>
			<p>A right to education has been recognized by some governments. At the global level, Article 13 of the United Nations' 1966 International Covenant on Economic, Social and Cultural Rights recognizes the right of everyone to an education.[2] Although education is compulsory in most places up to a certain age, attendance at school often isn't, and a minority of parents choose home-schooling, e-learning or similar for their children.</p>
		</div>
	</div>
	<div class="col-md-4 col-xs-12 pull-right">
		<div class="news">
			<h2>News and Events</h2>
		</div>
	</div>
</div>
</div><!--end rows-->



<div class="container" id="class">
	<div class="col-sm-3 ">
		<div class="classes"></div>
	</div>
	<div class="col-sm-3">
		<div class="classes"></div>
	</div>
	<div class="col-sm-3">
		<div class="classes"></div>
	</div>
	<div class="col-sm-3">
		<div class="classes"></div>
	</div>
</div>





	<div class="container" id="movingImg">
		<h2>Our Affiliations</h2>
		<ul id="scroller">
			<li><img src="img1.jpg" alt=""></li>
			<li><img src="img1.jpg" alt=""></li>
			<li><img src="img1.jpg" alt=""></li>
			<li><img src="img1.jpg" alt=""></li>
			<li><img src="img1.jpg" alt=""></li>
			<li><img src="img1.jpg" alt=""></li>
			<li><img src="img1.jpg" alt=""></li>
			<li><img src="img1.jpg" alt=""></li>
			<li><img src="img1.jpg" alt=""></li>
			<li><img src="img1.jpg" alt=""></li>
		</ul>
	</div>




<?php get_footer(); ?>