<?php

if ( function_exists('register_sidebar') ) {
	
	register_sidebar(array(
		'name'=>'Left Sidebar',
		'before_widget' => '<div id="%1$s" class="widget_box">',
		'after_widget' => '</div>',
		'before_title' => '<h2>',
		'after_title' => '</h2>',
	));
			
register_sidebar(array(
		'name'=>'Testimoials',
		'before_widget' => '<div id="%1$s" class="widget_box">',
		'after_widget' => '</div>',
		'before_title' => '<h2>',
		'after_title' => '</h2>',
	));
}


//error_reporting(E_ALL);
//ini_set('display_errors', '1');

function register_menus() {
	register_nav_menus();
}
add_action( 'init', 'register_menus' );

function excerpt($num) {
$limit = $num+1;
$excerpt = explode(' ', get_the_excerpt(), $limit);
array_pop($excerpt);
$excerpt = implode(" ",$excerpt)." ...";
echo $excerpt;
}



function cat_image($postid=0, $attributes='') {
	if ($postid<1) $postid = get_the_ID();
	if ($images = get_children(array(
		'post_parent' => $postid,
		'post_type' => 'attachment',
		'numberposts' => 1,
		'post_mime_type' => 'image',)))
		foreach($images as $image) {
			$attachment=wp_get_attachment_image_src($image->ID);
			?>
<img src="<?php echo $attachment[0]; ?>" <?php //echo $attributes; ?>  height="90" width="130"/><?php
		}
		else{
			
			echo('<img src="images/lights.jpg" alt="Landscape Travels" title="Landscape Travels" height="90" width="130"/>');
		}
		
}

if ( function_exists( 'add_theme_support' ) ) { // Added in 2.9
	add_theme_support( 'post-thumbnails' );
	
}

function set_flexslider_hg_rotators( $rotators = array() )
{
    $rotators['homepage']         = array( 'size' => 'homepage-rotator', 'heading_tag' => 'h2' );
    $rotators['contactus']        = array( 'size' => 'thumbnail', 'orderby' => 'title', 'order' => 'DESC' );
    $rotators['gallerypage']  = array( 'size' => 'medium', 'hide_slide_data' => true );
    $rotators['amenities']        = array( 'size' => '600', 'limit' => 5 );  
    return $rotators;
}

add_filter('flexslider_hg_rotators', 'set_flexslider_hg_rotators');
add_image_size( 'homepage-rotator', '530', '240', true );
$rotators['homepage'] = array( 'size' => 'homepage-rotator', 'options' => "{slideshowSpeed: 7000, direction: 'vertical'}" );

?>